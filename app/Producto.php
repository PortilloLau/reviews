<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    protected $fillable = [
        'nombre','imagen','cantidad_min','cantidad_max','codigo','proveedor_id','margen','precio_venta','status','categoria_id','subcategoria_id',
        'inpuesto_id','promocion_id','cantidad_producto'
    ];

	public function categoria()
	{
	    return $this->belongsTo(Categoria::class);
    }
    // public function subcategoria()
	// {
	//     return $this->belongsTo(Subcategoria::class);
    // }
    public function proveedor(){
        return $this->belongsTo(Proveedor::class); ///////////////////////////////////////////
    }
	public function impuesto()
    {
        return $this->belongsTo(Impuesto::class);
    }
    public function inventarios()
    {
        return $this->hasMany(InventarioProducto::class);
    }
    public function getPrecioVentaAttribute($value)
	{

	    return number_format($value/100,2,'.','');
	}
	public function setPrecioVentaAttribute($value)
    {
        //Redondear valores a $0.50 o $0.00 hacia arriba del precio fijado al momento de ingresar producto al inventario o fijado
        $d = ((floatval($value)*100)-(intval($value)*100));
        $value=$d>0 && $d<51? intval($value)+0.50:round($value);
        $this->attributes['precio_venta'] = $value*100;
    }

    public function ventas(){
	    return $this->belongsToMany(Venta::class)->withPivot(['producto_id','venta_id','precio','cantidad','vendido','promocion_id','descuento']);
    }

    public function combos(){
        return $this->belongsToMany(Combo::class)->withPivot(['combo_id','producto_id','cantidad']);
    }

    public function promocion(){
        return $this->belongsTo(Promocion::class);
    }
    //Medidas no fijas  ejemplo una caja de mazapan no tiene la misma cantidad de chocolates
    public  function  medidaProducto(){
	    return $this->hasMany(MedidaProducto::class,'codigo_id');
    }

    public function medida(){
	    return $this->belongsTo(Medida::class);
    }

    
    public function robos(){
	    return $this->belongsToMany(Robo::class);
    }

    public function ajustesInventario(){
	    return $this->hasMany(AjustesProducto::class);
    }
}
