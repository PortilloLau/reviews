<?php

namespace App\Http\Controllers\Ajax;

use App\Book;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Image;
use DB;

class PostController extends Controller
{

    // Books
    public function storeBooks(Request $request)
    {
        $request->validate([
            'title' => 'required|min:3|max:255',
            'author' => 'required|min:3|max:255',
            'page_number' => 'required|min:0',
            'language' => 'required|min:3|max:255',
            
        ]);

        $extension = $request->file('cover')->getClientOriginalExtension();
        $filename = str_random(10) . '.' . $extension;
        $img = Image::make($request->file('cover'))->save('../storage/app/public/' . $filename);
        $book = new Book;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->page_number = $request->page_number;
        $book->language = $request->language;
        $book->cover = $filename;

        $book->save();

        return response()->json($book);
    }
    public function updateBooks(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|min:3|max:255',
            'author' => 'required|min:3|max:255',
            'page_number' => 'required|min:0',
            'language' => 'required|min:3|max:255',
        ]);
        $producto = Book::findOrFail($id);
        $producto->title = $request->title;
        $producto->author = $request->author;
        $producto->page_number = $request->page_number;
        $producto->language = $request->language;
        $producto->save();
        return response()->json($request->all());
    }

    public function deleteBooks($id)
    {
        $book = Book::destroy($id);
        return response()->json($book);
        
    }

    //REVIEWS
    public function storeReviews(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required',
            'comment' => 'required|min:3|max:255',
            'book_id' => 'required',
            
        ]);

        $review = new Review;
        $review->name = $request->name;
        $review->email = $request->email;
        $review->comment = $request->comment;

        if ($request->book_id) {
            $review->book_id = $request->book_id;
        }

        $review->save();

        return response()->json($review);
    }
    public function updateReviews(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required',
            'comment' => 'required|min:3|max:255',
            'book_id' => 'required',
        ]);
        
        $review = Review::findOrFail($id);
        $review->name = $request->name;
        $review->email = $request->email;
        $review->comment = $request->comment;
        $review->book_id = $request->book_id;
        $review->save();
        return response()->json($request->all());
    }

    public function deleteReviews($id)
    {
        $review = Review::destroy($id);
        return response()->json($review);
        
    }

}
