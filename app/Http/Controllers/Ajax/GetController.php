<?php

namespace App\Http\Controllers\Ajax;

use App\Book;
use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Cache;
use DB;
use function PHPSTORM_META\map;

class GetController extends Controller
{

    public function getBooks()
    {
    	$books = Book::query();
    	return datatables($books)->toJson();
    }

    public function getReviews()
    {
        $reviews = DB::table('books')
            ->join('reviews', 'books.id', '=', 'reviews.book_id')
            ->select('reviews.*', 'books.title AS title')
            ->get();
            return datatables($reviews)->toJson();
    }

}
