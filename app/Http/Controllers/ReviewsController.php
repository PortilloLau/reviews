<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        $titles= Book::pluck('title', 'id');
        return view('review')->with('titles', $titles);
    }

}
