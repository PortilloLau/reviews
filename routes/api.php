<?php

use Illuminate\Http\Request;

//BOOKS
Route::post('books', 'Ajax\PostController@storeBooks')->name('ajax.books.store');
Route::put('books/{id}', 'Ajax\PostController@updateBooks')->name('ajax.books.update');
Route::delete('books/{id}', 'Ajax\PostController@deleteBooks')->name('ajax.books.delete');

//REVIEWS
Route::post('reviews', 'Ajax\PostController@storeReviews')->name('ajax.reviews.store');
Route::put('reviews/{id}', 'Ajax\PostController@updateReviews')->name('ajax.reviews.update');
Route::delete('reviews/{id}', 'Ajax\PostController@deleteReviews')->name('ajax.reviews.delete');

