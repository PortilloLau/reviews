<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    $acenstors = \App\Categoria::ancestorsAndSelf(3);
    dd($acenstors);
});
Auth::routes();
Route::get('/home', 'HomeController@index');
Route::prefix('admin')->group(function() {
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.auth.logout');
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
});

Route::get('books', 'Ajax\GetController@getBooks')->name('ajax.books.get');
Route::get('reviewsGet', 'Ajax\GetController@getReviews')->name('ajax.reviews.get');
Route::get('reviews', 'ReviewsController@index')->name('reviews.index');
