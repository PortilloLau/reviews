@extends('layouts.admin')

@section('css')

<!-- Datatables -->
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<!-- PNotify -->
<link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
<link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
<link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<!-- Select2 -->
<link href="/vendors/select2/dist/css/select2.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/build/css/custom.min.css" rel="stylesheet">
    </head>

@endsection


@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Reviews</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Reviews's List</h2>
        <ul class="nav navbar-right panel_toolbox">
          <a href="#" class="btn btn-success" id="nuevoRol"><i class="fa fa-plus"></i> Add Review</a>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped" cellspacing="0" width="100%" id="reviewsTable">
          <thead>
          <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Comment</th>
              <th>Book</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Comment</th>
              <th>Book</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal nuevo -->
<div class="modal" id="modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">New Review</h5>
      </div>
      <div class="modal-body">
        {!! Form::open(['id' => 'newForm', 'class' => 'form-label-left input_mask']) !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('new-name', '*Name') !!}
                    {!! Form::text('new-name', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-email', '*Email') !!}
                    {!! Form::text('new-email', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-comment', '*Comment') !!}
                    {!! Form::text('new-comment', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-book', '*Book') !!}
                    {!! Form::select('new-book', $titles, null, ['placeholder'=>'Select a Book', 'class' =>'form-control']) !!}
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="saveNuevo">Save</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- Modal edit -->
<div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Update Review</h5>
      </div>
      <div class="modal-body">
        {!! Form::open() !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Form::hidden('edit-id', null, ['id' => 'edit-id']) !!}
                    {!! Form::label('edit-name', '*Name') !!}
                    {!! Form::text('edit-name', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-email', '*Email') !!}
                    {!! Form::text('edit-email', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-comment', '*Comment') !!}
                    {!! Form::text('edit-comment', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-book', '*Book') !!}
                    {!! Form::select('edit-book', $titles, null, ['placeholder'=>'Select a Book', 'class' =>'form-control']) !!}
                </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="saveEdit"> Save</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- PNotify -->
<script src="/vendors/pnotify/dist/pnotify.js"></script>
<script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Swal -->
<script src="/js/sweetalert.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  
    var token = $('@csrf').val();
  var reviewsTable = $('#reviewsTable').DataTable( {
      
      "scrollX": true,
      "processing": true,
      "serverSide": true,
      "language": {
       "search":"Buscar",
       "emptyTable": "No hay información",
       "entries":"",
       "processing":"procesando",
       "info": "Mostrando PRINCIPIO a FIN de TOTAL Entradas",
       "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
       "infoFiltered": "(Filtrado de MAX total entradas)",
       "infoPostFix": "",
       "thousands": ",",
       "lengthMenu": "Mostrar _MENU_ Entradas",
       "loadingRecords": "Cargando...",
       "zeroRecords": "Sin resultados encontrados",
       "paginate": {
           "first": "Primero",
           "last": "Ultimo",
           "next": "Siguiente",
           "previous": "Anterior"
       },
     },
      "ajax": {
          url: "{!!route('ajax.reviews.get')!!}"
      },
      "columns": [
          { data: 'name', name: 'name' },
          { data: 'email', name: 'email' },
          { data: 'comment', name: 'comment' },
          { data: 'title', name: 'title' },
          { data: '', name: '' },

      ],
      "columnDefs": [
     
        {
          "autoWidth": true,
          "targets": 4,
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row) {
              return "<div class=''><a class='btn btn-primary edit-modal-toggler' href='javascript:void(0);'><i class='fa fa-lg fa-edit'></i></a><a class='btn btn-danger delete-confirmation-toggler' title='inactivar'  href='javascript:void(0);'><i class='fa fa-lg fa-ban'></i></a></div>"
          }
        }
    ],
  });

  //////////////////POST//////////////////
$('#nuevoRol').on('click', function () {
    $('#new-name').val("")
    $('#new-email').val("")
    $('#new-comment').val("")
    $('#modal-nuevo').modal();
  } );
  $('#saveNuevo').on('click', function () {
      swal({
          title: "New Review",
          text: "¿Are you sure?",
          icon: "info",
          buttons: {
            cancel: true,
            confirm: {
              text: "Save",
              closeModal: false
            }
          }
      })
      .then((val) => {
        if (val) {
          var $data = new FormData();
          $data.append('name', $('#new-name').val());
          $data.append('email', $('#new-email').val());
          $data.append('comment', $('#new-comment').val());
          $data.append('book_id', $('#new-book').val());

          $.ajax({
            url: "{{route('ajax.reviews.store')}}",
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            data: $data,
          })
            .done(function sucess(response) {
              console.log(response);
                $('#new-name').val("");
                $('#new-email').val("");
                $('#new-comment').val();
                reviewsTable.ajax.reload();
                $('#modal-nuevo').modal('hide');
                swal("¡Genial!", "¡Actualizado exitosamente!", "success");
            }).fail(function error(response) {
                swal("¡Ha ocurrido un error!", "Asegurate de haber llenado los campos necesarios y que el código de barras no se encuentre repetido", "error");
                console.log(response);
            });
        }else{
          swal.close();
        }
      });
  } );

  ///////UPDATE///////
  $('#reviewsTable').on('click', '.edit-modal-toggler', function () {
      var row = this.closest('tr');
      var data = reviewsTable.row(row).data();
        console.log(data)
      $('#edit-id').val(data.id);
      $('#edit-name').val(data.name);
      $('#edit-email').val(data.email);
      $('#edit-comment').val(data.comment);
      $('#edit-book').val(data.book_id);
      $('#modal-edit').modal()
  } );

  $('#saveEdit').on('click', function () {
    var edit_id = $('#edit-id').val();
    swal({
        title: "Confirmation",
        text: "¿Are you sure?",
        icon: "info",
        buttons: {
          cancel: true,
          confirm: {
            text: "Save",
            closeModal: false
          }
        }
    })
    .then((val) => {
      if (val) {
        var $data = new FormData();
        $data.append('_token', '{{ csrf_token() }}');
        $data.append('_method', 'PUT');
        $data.append('name', $('#edit-name').val());
        $data.append('email', $('#edit-email').val());
        $data.append('comment', $('#edit-comment').val());
        $data.append('book_id', $('#edit-book').val());

        $.ajax({
            url: "/api/reviews/"+edit_id,
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            data: $data,
          })
            .done(function sucess(response) {
                // console.log(response);
                reviewsTable.ajax.reload();
                $('#modal-edit').modal('hide');
                swal("¡Genial!", "¡Actualizado exitosamente!", "success");
            }).fail(function error(response) {
                swal("Ups...", "¡Ha ocurrido un error!", "error");
                // console.log(response);
            });
          }else{
            swal.close();
          }
    });
  } );

  ////////////DELETE///////////
  $('#reviewsTable').on('click', '.delete-confirmation-toggler', function () {
      var row = this.closest('tr');
      var data = reviewsTable.row(row).data();
      console.log(data);
      //Llenamos el modal edit
      var del_id = data.id;
      swal({
          title: "Confirmation",
          text: "¿Are you sure?",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "Delete",
              closeModal: false
            }
          }
      })
      .then((val) => {
        if (val) {
          var data = {"_token": "{{ csrf_token() }}", "_method": "DELETE"}
          $.post("/api/reviews/"+del_id, data)
              .done(function sucess(response) {
                  reviewsTable.ajax.reload();
                  swal("¡Genial!", "¡Eliminado exitosamente!", "success");
              }).fail(function error(response) {
                  swal("Ups...", "¡Ha ocurrido un error!", "error");
                  console.log(response);
              });
            }else{
              swal.close();
            }
      });         
  } );


});
@if(session('success'))
  new PNotify({
    title: '¡Genial!',
    text: "{{session('success')}}",
    type: 'success',
    styling: 'bootstrap3'
  });
@endif
</script>
@endsection
