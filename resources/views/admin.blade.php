@extends('layouts.admin')

@section('css')

<!-- Datatables -->
<link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<!-- PNotify -->
<link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
<link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
<link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<!-- Select2 -->
<link href="/vendors/select2/dist/css/select2.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <link href="/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/build/css/custom.min.css" rel="stylesheet">
    </head>

@endsection


@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Books</h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Book's List</h2>
        <ul class="nav navbar-right panel_toolbox">
          <a href="#" class="btn btn-success" id="nuevoRol"><i class="fa fa-plus"></i> Add Book</a>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped" cellspacing="0" width="100%" id="booksTable">
          <thead>
          <tr>
              <th>Nombre</th>
              <th>Código de barras</th>
              <th>Nombre</th>
              <th>Código de barras</th>
              <th>Código de barras</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nombre</th>
              <th>Código de barras</th>
              <th>Nombre</th>
              <th>Código de barras</th>
              <th>Código de barras</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal nuevo -->
<div class="modal" id="modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">New Book</h5>
      </div>
      <div class="modal-body">
        {!! Form::open(['id' => 'newForm', 'class' => 'form-label-left input_mask']) !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('new-title', '*Title') !!}
                    {!! Form::text('new-title', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-author', '*Author') !!}
                    {!! Form::text('new-author', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-page', '*Number of Pages') !!}
                    {!! Form::number('new-page', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-language', '*Language') !!}
                    {!! Form::text('new-language', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('new-cover', '*Cover') !!}
                    {!! Form::file('new-cover', null, ['class' =>'form-control']) !!}
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="saveNuevo">Save</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- Modal edit -->
<div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Update Book</h5>
      </div>
      <div class="modal-body">
        {!! Form::open() !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Form::hidden('edit-id', null, ['id' => 'edit-id']) !!}
                    {!! Form::label('edit-title', '*Title') !!}
                    {!! Form::text('edit-title', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-author', '*Author') !!}
                    {!! Form::text('edit-author', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-page', '*Number of Pages') !!}
                    {!! Form::number('edit-page', null, ['class' =>'form-control']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('edit-language', '*Language') !!}
                    {!! Form::text('edit-language', null, ['class' =>'form-control']) !!}
                </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="saveEdit"> Save</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>



@endsection

@section('js')

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- PNotify -->
<script src="/vendors/pnotify/dist/pnotify.js"></script>
<script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
<!-- Datatables -->
<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Swal -->
<script src="/js/sweetalert.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  
    var token = $('@csrf').val();
  var booksTable = $('#booksTable').DataTable( {
      
      "scrollX": true,
      "processing": true,
      "serverSide": true,
      "language": {
       "search":"Buscar",
       "emptyTable": "No hay información",
       "entries":"",
       "processing":"procesando",
       "info": "Mostrando PRINCIPIO a FIN de TOTAL Entradas",
       "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
       "infoFiltered": "(Filtrado de MAX total entradas)",
       "infoPostFix": "",
       "thousands": ",",
       "lengthMenu": "Mostrar _MENU_ Entradas",
       "loadingRecords": "Cargando...",
       "zeroRecords": "Sin resultados encontrados",
       "paginate": {
           "first": "Primero",
           "last": "Ultimo",
           "next": "Siguiente",
           "previous": "Anterior"
       },
     },
      "ajax": {
          url: "{!!route('ajax.books.get')!!}"
      },
      "columns": [
          { data: 'title', name: 'title' },
          { data: 'author', name: 'author' },
          { data: 'page_number', name: 'page_number' },
          { data: 'language', name: 'language' },
          { data: 'cover', name: 'cover' },
          { data: '', name: '' },

      ],
      "columnDefs": [
        {
          "autoWidth": true,
          "targets": 4,
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row) {
            
            return '<img height="100" src="uploads/productos/' +data+ '" />';
          }
        },
        {
          "autoWidth": true,
          "targets": 5,
          "orderable": false,
          "searchable": false,
          "render": function (data, type, row) {
              return "<div class=''><a class='btn btn-primary edit-modal-toggler' href='javascript:void(0);'><i class='fa fa-lg fa-edit'></i></a><a class='btn btn-danger delete-confirmation-toggler' title='inactivar'  href='javascript:void(0);'><i class='fa fa-lg fa-ban'></i></a></div>"
          }
        }
    ],
  });

//////////////////POST//////////////////
$('#nuevoRol').on('click', function () {
    $('#new-title').val("")
    $('#new-author').val("")
    $('#new-page').val()
    $('#new-language').val("")
    $('#new-cover').val("")
    $('#modal-nuevo').modal();
  } );
  $('#saveNuevo').on('click', function () {
      swal({
          title: "New Book",
          text: "¿Estás seguro de guardar los datos?",
          icon: "info",
          buttons: {
            cancel: true,
            confirm: {
              text: "Guardar",
              closeModal: false
            }
          }
      })
      .then((val) => {
        if (val) {
          var $data = new FormData();
          $data.append('title', $('#new-title').val());
          $data.append('author', $('#new-author').val());
          $data.append('page_number', $('#new-page').val());
          $data.append('language', $('#new-language').val());
          $data.append('cover', $('#new-cover').get(0).files[0]);

          $.ajax({
            url: "{{route('ajax.books.store')}}",
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            data: $data,
          })
            .done(function sucess(response) {
              console.log(response);
                $('#new-title').val("");
                $('#new-author').val("");
                $('#new-page').val();
                $('#new-language').val("")
                booksTable.ajax.reload();
                $('#modal-nuevo').modal('hide');
                swal("¡Genial!", "¡Actualizado exitosamente!", "success");
            }).fail(function error(response) {
                swal("¡Ha ocurrido un error!", "Asegurate de haber llenado los campos necesarios y que el código de barras no se encuentre repetido", "error");
                console.log(response);
            });
        }else{
          swal.close();
        }
      });
  } );
  
  ////////////UPDATE///////////////
  $('#booksTable').on('click', '.edit-modal-toggler', function () {
      var row = this.closest('tr');
      var data = booksTable.row(row).data();
        console.log(data)
      $('#edit-id').val(data.id);
      $('#edit-title').val(data.title);
      $('#edit-author').val(data.author);
      $('#edit-page').val(data.page_number);
      $('#edit-language').val(data.language);
      $('#modal-edit').modal()
  } );

  $('#saveEdit').on('click', function () {
    var edit_id = $('#edit-id').val();
    swal({
        title: "Confirmar edición",
        text: "¿Estás seguro de modificar los datos?",
        icon: "info",
        buttons: {
          cancel: true,
          confirm: {
            text: "Guardar",
            closeModal: false
          }
        }
    })
    .then((val) => {
      if (val) {
        var $data = new FormData();
        $data.append('_token', '{{ csrf_token() }}');
        $data.append('_method', 'PUT');
        $data.append('title', $('#edit-title').val());
        $data.append('author', $('#edit-author').val());
        $data.append('page_number', $('#edit-page').val());
        $data.append('language', $('#edit-language').val());

        $.ajax({
            url: "/api/books/"+edit_id,
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            data: $data,
          })
            .done(function sucess(response) {
                // console.log(response);
                booksTable.ajax.reload();
                $('#modal-edit').modal('hide');
                swal("¡Genial!", "¡Actualizado exitosamente!", "success");
            }).fail(function error(response) {
                swal("Ups...", "¡Ha ocurrido un error!", "error");
                // console.log(response);
            });
          }else{
            swal.close();
          }
    });
  } );

  ////////////DELETE///////////
  $('#booksTable').on('click', '.delete-confirmation-toggler', function () {
      var row = this.closest('tr');
      var data = booksTable.row(row).data();
      console.log(data);
      //Llenamos el modal edit
      var del_id = data.id;
      swal({
          title: "Confirmar eliminación",
          text: "¿Estás seguro de eliminar el producto?",
          icon: "warning",
          buttons: {
            cancel: true,
            confirm: {
              text: "Borrar",
              closeModal: false
            }
          }
      })
      .then((val) => {
        if (val) {
          var data = {"_token": "{{ csrf_token() }}", "_method": "DELETE"}
          $.post("/api/books/"+del_id, data)
              .done(function sucess(response) {
                  booksTable.ajax.reload();
                  swal("¡Genial!", "¡Eliminado exitosamente!", "success");
              }).fail(function error(response) {
                  swal("Ups...", "¡Ha ocurrido un error!", "error");
                  console.log(response);
              });
            }else{
              swal.close();
            }
      });         
  } );

});
@if(session('success'))
  new PNotify({
    title: '¡Genial!',
    text: "{{session('success')}}",
    type: 'success',
    styling: 'bootstrap3'
  });
@endif
</script>
@endsection
