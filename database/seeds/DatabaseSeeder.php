<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
   
        $this->users();
        $this->admins();
        $this->roles();
        $this->role_user();
 
        $this->call([

        ]);
    }

    function users(){
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('password')
         ]);    
    }

     function admins(){
        DB::table('admins')->insert([
            'name'=>'admin',
            'username'=>'admin',
            'password'=>bcrypt('password')
        ]);
    }

    function roles(){
        DB::table('roles')->insert([
            'name'=>'Administrador',
            'display_name'=>'admin',
            'description'=>'Administrador del sitio'
        ]);
    }

    public function role_user()
    {
        DB::table('role_user')->insert([
            'role_id'=>'1',
            'user_id'=>'1',
            'user_type'=>'admin'
        ]);
    }
}
