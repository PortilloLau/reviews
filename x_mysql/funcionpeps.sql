-------------------------------------------INVENTARIO INGREDIENTES----------------------------------------------------
-- parametros: IN vfecha DATE, IN vdetalle VARCHAR(191), IN vcantidad INT, IN vprecio_compra INT, 
-- IN vprecio_inventario INT, IN vingrediente_id INT, IN vtipomovimiento INT
BEGIN
        DECLARE vcantidad_inv, vprecio_compra_inv,vprecio_total_inv INT DEFAULT 0;
        DECLARE var_cantidad,var_id,var_precio,var_restante INT;
        IF vtipomovimiento=1 THEN -- -------   ENTRADAS -------
                SET vcantidad_inv=vcantidad;
                SET vprecio_compra_inv=vprecio_compra;
                SET vprecio_total_inv=vprecio_inventario;
                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,                 cantidad,precio_compra,precio_inventario,cantidad_inventario,
                        precio_compra_inventario,total_inventario,ingrediente_id,
                        status_saldo,created_at,updated_at) VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra,
                vprecio_inventario,vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vingrediente_id,0,now(),now());
        ELSEIF vtipomovimiento=2 THEN  -- ELSEIF PRINCIPAL -- SALIDAS-----
            IF(vcantidad<=(Select SUM(cantidad_inventario)from inventario_ingredientes 
                WHERE (status_saldo=0 OR status_saldo=2) AND ingrediente_id=vingrediente_id)) THEN
                IF(EXISTS(SELECT*FROM inventario_ingredientes WHERE status_saldo=2 )) THEN
                        SET var_id= (Select id from inventario_ingredientes 
                                WHERE status_saldo=2 AND ingrediente_id=vingrediente_id);
                        SET var_cantidad=(Select cantidad_inventario from inventario_ingredientes 
                                WHERE status_saldo=2 AND ingrediente_id=vingrediente_id);
                        SET var_precio=(Select precio_compra_inventario 
                                from inventario_ingredientes WHERE status_saldo=2 AND ingrediente_id=vingrediente_id);
                        IF(vcantidad<var_cantidad)THEN
                                UPDATE inventario_ingredientes SET status_saldo=1 
                                WHERE id=var_id;
                                SET vcantidad_inv=var_cantidad-vcantidad;
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
                                (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vingrediente_id,2,now(),now());
                        END IF;
                        IF(vcantidad=var_cantidad) THEN
                                UPDATE inventario_ingredientes SET status_saldo=1
                                WHERE id=var_id;
                                SET vcantidad_inv=var_cantidad-vcantidad;
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
                                (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vingrediente_id,3,now(),now());
                        END IF;
                        IF vcantidad>var_cantidad THEN -- en caso de que sea mayor cant puesta
                                UPDATE inventario_ingredientes SET status_saldo=1
                                        WHERE id=var_id;
                                SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                        INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*vprecio_compra_inv),                      			vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                                SET var_restante=vcantidad-var_cantidad; -- puesta- cantInv anterior
                                WHILE var_restante>0 DO -- ------------------------------------------
                                        SET var_id= (Select id from inventario_ingredientes 
                                                WHERE status_saldo=0 AND ingrediente_id=vingrediente_id ORDER BY fecha LIMIT 1);
                                        SET var_cantidad=(Select cantidad_inventario from 		   					inventario_ingredientes WHERE status_saldo=0 AND ingrediente_id=vingrediente_id
                                                        ORDER BY fecha ASC LIMIT 1); 
                                        SET var_precio=(Select precio_compra_inventario from 				  		  inventario_ingredientes WHERE status_saldo=0 AND ingrediente_id=vingrediente_id
                                                ORDER BY fecha ASC LIMIT 1);
                                        IF var_restante=var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_restante; -- debe ser 0
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*var_precio),                      			   	  vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                                        END IF;
                                        IF var_restante<var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_restante; -- 
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- 
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_restante,                                    var_precio,(var_restante*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,           			vingrediente_id,2,now(),now());
                                        END IF;
                                        IF var_restante>var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0 1-2=-1
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	var_precio,(var_cantidad*var_precio),                      			   	 vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                                        END IF;
                                                -- 0  = 2 - 3
                                        SET var_restante=var_restante-var_cantidad;-- restant- cantInvAnterior
                                END WHILE;	
        -- -----------------------------------------------------------------
                        END IF; -- FIN DE IF CON WHILE  
                ELSE
                        SET var_id= (Select id from inventario_ingredientes 
                                WHERE status_saldo=0 AND ingrediente_id=vingrediente_id order by fecha ASC LIMIT 1);
                        SET var_cantidad=(Select cantidad_inventario from inventario_ingredientes 
                                WHERE status_saldo=0 AND ingrediente_id=vingrediente_id order by fecha ASC LIMIT 1);
                        SET var_precio=(Select precio_compra_inventario from 			           		   inventario_ingredientes 
                                WHERE status_saldo=0 AND ingrediente_id=vingrediente_id order by fecha ASC LIMIT 1);
                        IF(vcantidad<var_cantidad)THEN
                                UPDATE inventario_ingredientes SET status_saldo=1 
                                        WHERE id=var_id;
                                SET vcantidad_inv=var_cantidad-vcantidad;
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 						       VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
                                        (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vingrediente_id,2,now(),now());
                        END IF;
                        IF(vcantidad=var_cantidad) THEN
                                UPDATE inventario_ingredientes SET status_saldo=1 
                                WHERE id=var_id;
                                SET vcantidad_inv=vcantidad-var_cantidad; -- debe ser 0
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                        INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,                                var_precio,(vcantidad*var_precio),                      			       vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                        END IF;
                        IF vcantidad>var_cantidad THEN
                                UPDATE inventario_ingredientes SET status_saldo=1
                                                WHERE id=var_id;
                                SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0
                                SET vprecio_compra_inv=var_precio;
                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                        INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,vdetalle,vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                                SET var_restante=vcantidad-var_cantidad; -- puesta- cantInv anterior
                                WHILE var_restante>0 DO -- ------------------------------------------
                                        SET var_id= (Select id from inventario_ingredientes 
                                                WHERE status_saldo=0 AND ingrediente_id=vingrediente_id ORDER BY fecha LIMIT 1);
                                        SET var_cantidad=(Select cantidad_inventario from 		   					inventario_ingredientes WHERE status_saldo=0 AND ingrediente_id=vingrediente_id
                                                        ORDER BY fecha ASC LIMIT 1); 
                                        SET var_precio=(Select precio_compra_inventario from 				  		  inventario_ingredientes WHERE status_saldo=0 AND ingrediente_id=vingrediente_id
                                                ORDER BY fecha ASC LIMIT 1);
                                        IF var_restante=var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_restante; -- debe ser 0
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	   var_precio,(var_cantidad*var_precio),                      			   	vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                       vingrediente_id,3,now(),now());
                                        END IF;
                                        IF var_restante<var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_restante; -- 
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- 
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 						       VALUES(vfecha,'',vtipomovimiento,var_restante,                                    var_precio,(var_restante*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,           			vingrediente_id,2,now(),now());
                                        END IF;
                                        IF var_restante>var_cantidad THEN
                                                UPDATE inventario_ingredientes SET status_saldo=1
                                                                WHERE id=var_id;
                                                SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0 1-2=-1
                                                SET vprecio_compra_inv=var_precio;
                                                SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
                                                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
                                                        precio_compra_inventario,total_inventario,ingrediente_id,
                                                        status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	    var_precio,(var_cantidad*var_precio),                      			   	 vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vingrediente_id,3,now(),now());
                                        END IF;
                                        SET var_restante=var_restante-var_cantidad;
                                END WHILE;
                        END IF;
                END IF; --   //FIN DE IF(EXISTS) PRNCIPAL
            ELSE
                SELECT 'No hay productos suficientes' AS mensaje;
            END IF;
        ELSE -- ------------------------INVENTARIO -------------------------------
                SET vcantidad_inv=vcantidad;
                SET vprecio_compra_inv=vprecio_compra;
                SET vprecio_total_inv=vprecio_inventario;-- siempre son iguales
                INSERT INTO inventario_ingredientes(fecha, detalle,tipomovimiento_id,                 cantidad,precio_compra,precio_inventario,cantidad_inventario,
                        precio_compra_inventario,total_inventario,ingrediente_id,
                        status_saldo,created_at,updated_at) VALUES(vfecha,vdetalle,vtipomovimiento,0,0,
                        0,vcantidad,vprecio_compra,                                               vprecio_total_inv,vingrediente_id,0,now(),now());
        END IF;
END


-------------------------------------------------------INVENTARIO PRODUCTOS------------------------------------------------------------------------------------

-- parametros: IN vfecha DATE, IN vdetalle VARCHAR(191), IN vcantidad INT, IN vprecio_compra INT, 
-- IN vprecio_inventario INT, IN vproducto_id INT, IN vtipomovimiento INT
BEGIN
DECLARE vcantidad_inv, vprecio_compra_inv,vprecio_total_inv INT DEFAULT 0;
DECLARE var_cantidad,var_id,var_precio,var_restante INT;
IF vtipomovimiento=1 THEN -- -------   ENTRADAS -------
SET vcantidad_inv=vcantidad;
SET vprecio_compra_inv=vprecio_compra;
SET vprecio_total_inv=vprecio_inventario;
INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,                 cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra,
       vprecio_inventario,vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vproducto_id,0,now(),now());
ELSEIF vtipomovimiento=2 THEN  -- ELSEIF PRINCIPAL -- SALIDAS-----
IF(vcantidad<=(Select SUM(cantidad_inventario)from inventario_productos 
                WHERE (status_saldo=0 OR status_saldo=2) AND producto_id=vproducto_id)) THEN
IF(EXISTS(SELECT*FROM inventario_productos WHERE status_saldo=2  AND producto_id=vproducto_id)) THEN
SET var_id= (Select id from inventario_productos 
    	WHERE status_saldo=2  AND producto_id=vproducto_id);
SET var_cantidad=(Select cantidad_inventario from inventario_productos 
    	WHERE status_saldo=2 AND producto_id=vproducto_id);
SET var_precio=(Select precio_compra_inventario 
               from inventario_productos WHERE status_saldo=2  AND producto_id=vproducto_id);
 IF(vcantidad<var_cantidad)THEN
	UPDATE inventario_productos SET status_saldo=1 
    WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-vcantidad;
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
       (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vproducto_id,2,now(),now());
 END IF;
 IF(vcantidad=var_cantidad) THEN
 	UPDATE inventario_productos SET status_saldo=1
    WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-vcantidad;
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
       (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vproducto_id,3,now(),now());
 END IF;
 IF vcantidad>var_cantidad THEN -- en caso de que sea mayor cant puesta
 	UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
	INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 						VALUES(vfecha,vdetalle,vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*vprecio_compra_inv),                      			vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
    SET var_restante=vcantidad-var_cantidad; -- puesta- cantInv anterior
    WHILE var_restante>0 DO -- ------------------------------------------
    SET var_id= (Select id from inventario_productos 
    	WHERE status_saldo=0  AND producto_id=vproducto_id 
        ORDER BY fecha LIMIT 1);
    SET var_cantidad=(Select cantidad_inventario from 		   					inventario_productos WHERE status_saldo=0  AND producto_id=vproducto_id
  		ORDER BY fecha ASC LIMIT 1); 
    SET var_precio=(Select precio_compra_inventario from 				  		  inventario_productos WHERE status_saldo=0 AND producto_id=vproducto_id
        ORDER BY fecha ASC LIMIT 1);
    IF var_restante=var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_restante; -- debe ser 0
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*var_precio),                      			   	  vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
    END IF;
    IF var_restante<var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_restante; -- 
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- 
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_restante,                                    var_precio,(var_restante*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,           			vproducto_id,2,now(),now());
    END IF;
    IF var_restante>var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0 1-2=-1
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	var_precio,(var_cantidad*var_precio),                      			   	 vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
    END IF;
        -- 0  = 2 - 3
    SET var_restante=var_restante-var_cantidad;-- restant- cantInvAnterior
    END WHILE;	
    -- -----------------------------------------------------------------
 END IF; -- FIN DE if(exist)  
ELSE
SET var_id= (Select id from inventario_productos 
    	WHERE status_saldo=0  AND producto_id=vproducto_id 
        order by fecha ASC LIMIT 1);
SET var_cantidad=(Select cantidad_inventario from inventario_productos 
    	WHERE status_saldo=0  AND producto_id=vproducto_id
        order by fecha ASC LIMIT 1);
SET var_precio=(Select precio_compra_inventario from 			           		   inventario_productos 
    	WHERE status_saldo=0  AND producto_id=vproducto_id
        order by fecha ASC LIMIT 1);
 IF(vcantidad<var_cantidad)THEN
	UPDATE inventario_productos SET status_saldo=1 
    WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-vcantidad;
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 						       VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,vprecio_compra_inv,
          (vcantidad*vprecio_compra_inv),vcantidad_inv,vprecio_compra_inv,                      vprecio_total_inv,vproducto_id,2,now(),now());
 END IF;
 IF(vcantidad=var_cantidad) THEN
 	UPDATE inventario_productos SET status_saldo=1 
    WHERE id=var_id;
    SET vcantidad_inv=vcantidad-var_cantidad; -- debe ser 0
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
	INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,vdetalle,vtipomovimiento,vcantidad,                                var_precio,(vcantidad*var_precio),                      			       vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
 END IF;
 
 IF vcantidad>var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
	INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,vdetalle,vtipomovimiento,var_cantidad,                              var_precio,(var_cantidad*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
    SET var_restante=vcantidad-var_cantidad; -- puesta- cantInv anterior
    WHILE var_restante>0 DO -- ------------------------------------------
    SET var_id= (Select id from inventario_productos 
    	WHERE status_saldo=0  AND producto_id=vproducto_id
        ORDER BY fecha LIMIT 1);
    SET var_cantidad=(Select cantidad_inventario from 		   					inventario_productos WHERE status_saldo=0  AND producto_id=vproducto_id
  		ORDER BY fecha ASC LIMIT 1); 
    SET var_precio=(Select precio_compra_inventario from 				  		  inventario_productos WHERE status_saldo=0  AND producto_id=vproducto_id
        ORDER BY fecha ASC LIMIT 1);
    IF var_restante=var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_restante; -- debe ser 0
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	   var_precio,(var_cantidad*var_precio),                      			   	vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                       vproducto_id,3,now(),now());
    END IF;
    IF var_restante<var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_restante; -- 
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- 
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 						       VALUES(vfecha,'',vtipomovimiento,var_restante,                                    var_precio,(var_restante*var_precio),                      			   vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,           			vproducto_id,2,now(),now());
    END IF;
    IF var_restante>var_cantidad THEN
    UPDATE inventario_productos SET status_saldo=1
    		WHERE id=var_id;
    SET vcantidad_inv=var_cantidad-var_cantidad; -- debe ser 0 1-2=-1
    SET vprecio_compra_inv=var_precio;
    SET vprecio_total_inv=vcantidad_inv*vprecio_compra_inv;-- debe ser 0
    INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,             cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) 							VALUES(vfecha,'',vtipomovimiento,var_cantidad,                              	    var_precio,(var_cantidad*var_precio),                      			   	 vcantidad_inv,vprecio_compra_inv,vprecio_total_inv,                        vproducto_id,3,now(),now());
    END IF;
    SET var_restante=var_restante-var_cantidad;
    END WHILE;
 END IF;
END IF; --   //FIN DE ELSEIF PRNCIPAL
ELSE
        SIGNAL SQLSTATE 'HY000'
        SET MESSAGE_TEXT='quantity_exceeds_max_allowed';
END IF;
ELSE -- ------------------------INVENTARIO -------------------------------
SET vcantidad_inv=vcantidad;
SET vprecio_compra_inv=vprecio_compra;
SET vprecio_total_inv=vprecio_inventario;-- siempre son iguales
INSERT INTO inventario_productos(fecha, detalle,tipomovimiento_id,                 cantidad,precio_compra,precio_inventario,cantidad_inventario,
            precio_compra_inventario,total_inventario,producto_id,
            status_saldo,created_at,updated_at) VALUES(vfecha,vdetalle,vtipomovimiento,0,0,
            0,vcantidad,vprecio_compra,                                               vprecio_total_inv,vproducto_id,0,now(),now());
END IF;
END