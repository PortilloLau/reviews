-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-03-2020 a las 05:54:19
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `booksdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '$2y$10$FDq1rkl3vwFA1Z30FGAMUOmYA2z8.tHhY3bBehPVSk.IQRtwJpg8i', 'PGeQBmwvFf0i8tax4KH4LRtC7zmuaG42AmrVGho81vVUx7hKiR8kZbmEUB0c', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_number` int(11) NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `page_number`, `language`, `cover`, `created_at`, `updated_at`) VALUES
(3, 'test3', 'test3', 300, 'test3', 'rRFWedF8vp.png', '2020-03-15 23:38:08', '2020-03-16 00:00:04'),
(5, 'test', 'test', 100, 'test', '88JQhzIQX6.png', '2020-03-16 01:58:31', '2020-03-16 01:58:31'),
(6, 'asads', 'aaaa', 2324, 'aaaa', 'A8atXXJ0kp.png', '2020-03-16 04:10:26', '2020-03-16 04:10:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_09_24_221529_laratrust_setup_tables', 1),
(3, '2020_03_15_134117_create_users_table', 1),
(4, '2020_03_15_141055_create_books_table', 2),
(5, '2020_03_15_141159_create_reviews_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad_min` int(11) NOT NULL,
  `cantidad_max` int(11) NOT NULL,
  `proveedor_id` int(10) UNSIGNED DEFAULT NULL,
  `margen` int(11) NOT NULL,
  `precio_compra` decimal(10,0) NOT NULL,
  `precio_venta` decimal(11,0) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `tipo_receta` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `impuesto_id` int(10) UNSIGNED DEFAULT NULL,
  `promocion_id` int(10) UNSIGNED DEFAULT NULL,
  `cantidad_producto` int(10) UNSIGNED DEFAULT NULL,
  `medida_id` int(10) UNSIGNED DEFAULT NULL,
  `subcategoria_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `nombre`, `imagen`, `cantidad_min`, `cantidad_max`, `proveedor_id`, `margen`, `precio_compra`, `precio_venta`, `status`, `categoria_id`, `tipo_receta`, `created_at`, `updated_at`, `impuesto_id`, `promocion_id`, `cantidad_producto`, `medida_id`, `subcategoria_id`) VALUES
(47, '7567', 'Shampoo', 'aeMyJ7oH7E.PNG', 1, 250, 3, 30, '0', '9836', 0, 6, 1, '2019-10-11 03:56:38', '2019-11-23 06:20:09', 1, 3, NULL, 3, 7),
(52, '7501010506207', 'Platos desechables', 'kxaskcdFD.PNG', 1, 250, 2, 30, '0', '1000', 0, 5, 0, '2019-10-23 05:21:36', '2019-10-23 05:21:36', 1, NULL, NULL, 3, 6),
(53, '7501000624683', 'galletas', 'hjfghg.png', 5, 250, 2, 20, '0', '1600', 0, 2, 0, '2019-10-23 05:23:50', '2019-10-24 01:11:22', 1, NULL, 5, 1, 8),
(54, '7501049402013', 'tenedores desechables', 'jfgfjg.png', 6, 250, 2, 30, '0', '3550', 0, 5, 0, '2019-10-23 05:25:14', '2019-11-14 07:29:22', 1, 1, NULL, 3, 6),
(55, '7501035910034', 'Fabuloso', 'kjdksd', 7, 200, 3, 30, '0', '5000', 0, 2, 0, '2019-10-23 05:32:06', '2019-10-23 05:32:06', 1, NULL, 5, 1, 5),
(56, '7501003339454', 'Te mcCormick hiervabuena', 'kfjkg', 10, 200, 4, 30, '0', '1200', 0, 2, 0, '2019-10-23 05:32:59', '2019-10-23 05:39:35', 1, NULL, 8, 3, 8),
(57, '7501003339133', 'Te mcCormick manzanilla', 'kfjgfklj', 10, 200, 4, 30, '0', '1400', 0, 2, 0, '2019-10-23 05:33:41', '2019-10-23 05:33:41', 1, NULL, 1, 3, 8),
(58, '7501003339522', 'Te mcCormick canela', NULL, 10, 200, 4, 30, '0', '1400', 0, 2, 0, '2019-10-23 05:35:09', '2019-10-23 05:39:09', 1, NULL, NULL, 3, 8),
(59, '7501058620101', 'Nescafe clasico', NULL, 1, 250, 5, 30, '0', '2300', 0, 2, 0, '2019-10-23 05:37:06', '2019-10-23 05:37:06', 1, NULL, NULL, 1, 8),
(60, '7501821512046', 'Aromatizante wiese', 'tyhtyj', 1, 250, 3, 30, '0', '4800', 0, 2, 0, '2019-10-23 05:38:16', '2019-10-24 01:11:04', 1, NULL, NULL, 3, 5),
(61, '7501073831469', 'Yogurth de manzana', NULL, 15, 250, 2, 30, '0', '1700', 0, 2, 0, '2019-10-23 05:42:19', '2019-10-23 05:42:54', 1, NULL, NULL, 1, 3),
(62, '098765', 'Mermelada', NULL, 1, 250, 4, 30, '0', '35', 0, 5, 0, '2019-10-24 00:53:30', '2019-11-22 01:28:26', 1, NULL, NULL, 1, 3),
(63, '91827', 'Chocolate', NULL, 1, 250, 3, 30, '0', '2350', 0, 2, 0, '2019-10-24 04:35:11', '2019-10-24 04:35:11', 1, NULL, NULL, 1, 3),
(64, '7501000133031', 'Barritas de piña', NULL, 6, 250, 2, 20, '0', '3450', 0, 2, 0, '2019-10-24 04:51:47', '2019-10-24 04:51:47', 1, NULL, NULL, 5, 8),
(65, '6121824', 'galletas marin', NULL, 1, 255, 1, 30, '0', '1000', 0, 1, 0, '2019-11-06 16:00:00', '2019-11-06 16:00:00', 1, NULL, NULL, 1, 0),
(66, '6121824', 'galletas marin', NULL, 1, 255, 1, 30, '0', '1000', 0, 1, 0, '2019-11-06 16:00:00', '2019-11-06 16:00:00', 1, NULL, NULL, 1, 1),
(67, '7777', 'barritas', NULL, 1, 250, 2, 30, '10', '14', 1, 2, 0, '2019-11-20 03:54:07', '2019-11-20 03:54:07', 1, NULL, NULL, 1, NULL),
(68, '8888', 'café', NULL, 1, 250, 5, 30, '50', '55', 1, 2, 0, '2019-11-20 04:12:37', '2019-11-22 01:29:25', 1, NULL, NULL, 1, NULL),
(70, '9999', 'vaso desechable 12', NULL, 1, 250, 3, 30, '30', '45', 1, 2, 0, '2019-11-22 00:38:09', '2019-11-22 00:38:09', 1, NULL, NULL, 1, NULL),
(71, '3333', 'GOMITAS', NULL, 1, 250, 2, 30, '5', '7', 1, 2, 0, '2019-11-22 04:31:44', '2019-11-22 04:31:44', 1, NULL, NULL, 1, NULL),
(72, '9898', 'té de manzana con canela', NULL, 1, 250, 3, 30, '25', '36', 1, 2, 0, '2019-11-23 11:52:15', '2019-11-23 11:52:15', 1, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `email`, `comment`, `book_id`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@test', 'asdsafafd', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'admin', 'Administrador del sitio', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$zOOgamltMTfKs9pzrMoGPOU2Oe/NeHiAyJjUl5JtrvQ8E2ASJt/X.', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indices de la tabla `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indices de la tabla `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_book_id_foreign` (`book_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
